package com.cogent;

import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.Assert.assertTrue;

public class ApplicationTest {

    @Test
    public void testApplication() throws Exception {
        ClassLoader classLoader = getClass().getClassLoader();
        Path res = Paths.get(classLoader.getResource("1.jpg").getPath());
        String[] args = new String[] { res.getParent().toFile().getAbsolutePath() };
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        System.setOut(new PrintStream(out));
        Application.main(args);
        String output = out.toString();
        ScannerTest.EXPECTED_RESULTS.stream().forEach(path -> assertTrue(output.contains(path.toFile().getAbsolutePath())));
        out.close();
        System.setOut(System.out);
    }
}
