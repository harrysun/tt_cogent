package com.cogent;

import org.junit.Test;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ScannerTest {

    private static final ClassLoader CLASS_LOADER = ScannerTest.class.getClassLoader();

    public static final Set<Path> EXPECTED_RESULTS = new HashSet<Path>() { {
        add(Paths.get(CLASS_LOADER.getResource("1.jpg").getPath()));
        add(Paths.get(CLASS_LOADER.getResource("2.jpg").getPath()));
        add(Paths.get(CLASS_LOADER.getResource("3.jpg").getPath()));
        add(Paths.get(CLASS_LOADER.getResource("5/a.jpg").getPath()));
    } };

    @Test
    public void testScan() throws Exception {
        Path path = Paths.get(CLASS_LOADER.getResource("1.jpg").getPath());
        Scanner scanner = new Scanner(path.getParent().toFile().getAbsolutePath());
        Map<String, Set<Path>> result = scanner.findDuplicated();
        assertEquals(result.size(), 1);
        List<Set<Path>> list = new ArrayList<>();
        list.addAll(result.values());
        assertEquals(list.size(), 1);
        Set<Path> actual = list.get(0);
        assertTrue(actual.containsAll(EXPECTED_RESULTS));
    }
}
