package com.cogent;

import org.junit.Test;

import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.Assert.assertEquals;

public class UtilsTest {

    @Test
    public void testBytes2Hex() {
        byte[] value = new byte[] { 0x01, (byte) 0xF1, 0x00, 0x55 };
        String hex = Utils.bytes2Hex(value);
        assertEquals("method bytes2Hex fails", hex, "01f10055");
    }

    @Test
    public void testMd5() throws Exception {
        ClassLoader classLoader = getClass().getClassLoader();
        Path path = Paths.get(classLoader.getResource("4.jpg").getPath());
        assertEquals("method md5 fails!", Utils.md5(path), "15d3a7df86e71166ab4b1bd0bfd258f1");
    }
}
