package com.cogent;

import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Scanner {

    private static final Logger LOGGER = Logger.getLogger(Application.class.getName());

    private Path photoPath;


    public Scanner(String path) {
        photoPath = Paths.get(path);
    }

    public Map<String, Set<Path>> findDuplicated() throws IOException {
        Map<String, Set<Path>> filesGroupByHash = new HashMap<>();
        Map<Long, Set<Path>> filesGroupBySize = new HashMap<>();
        scanBySize(photoPath, filesGroupBySize);
        filesGroupBySize.forEach((size, sizeGroup) -> {
            if (sizeGroup.size() <= 1) return;
            sizeGroup.stream().forEach(path -> {
                try {
                    String hash = Utils.md5(path);
                    Set<Path> hashGroup = filesGroupByHash.get(hash);
                    if (hashGroup == null) {
                        hashGroup = new HashSet<>();
                        filesGroupByHash.put(hash, hashGroup);
                    }
                    hashGroup.add(path);
                } catch (Exception ex) {
                    LOGGER.log(Level.WARNING, "Failed to calculate md5 checksum for: " + path, ex);
                }
            });
        });
        Map<String, Set<Path>> result = new HashMap<>();
        filesGroupByHash.forEach((hash, hashGroup) -> {
            if (hashGroup.size() <= 1) return;
            result.put(hash, hashGroup);
        });
        return result;
    }

    /**
     * scan all files in the path and group files by its size
     * @return
     */
    private void scanBySize(Path scanPath, Map<Long, Set<Path>> scanResult) throws IOException {
        DirectoryStream<Path> stream = Files.newDirectoryStream(scanPath);
        stream.forEach(path -> {
            try {
                if (Files.isDirectory(path)) {
                    scanBySize(path, scanResult);
                } else if (Files.isRegularFile(path)) {
                    FileChannel channel = FileChannel.open(path);
                    long size = channel.size();
                    Set<Path> group = scanResult.get(size);
                    if (group == null) {
                        group = new HashSet<>();
                        scanResult.put(size, group);
                    }
                    group.add(path);
                }
            } catch (IOException ex) {
                LOGGER.log(Level.WARNING, "Failed to process: " + path, ex);
            }
        });
    }

}
