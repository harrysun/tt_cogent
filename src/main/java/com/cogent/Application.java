package com.cogent;

import java.nio.file.Path;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Application {

    private static final Logger LOGGER = Logger.getLogger(Application.class.getName());

    public static void main(String[] args) {
        try {
            String scanPath = args.length > 0 ? args[0] : null;
            Scanner scanner = new Scanner(scanPath);
            Map<String, Set<Path>> duplicates = scanner.findDuplicated();
            duplicates.forEach((hash, group) -> {
                System.out.println("Duplicated files found with same md5 checksum[" + hash + "]:");
                group.stream().forEach(path -> System.out.println("\t" + path.toFile().getAbsolutePath()));
            });
        } catch (Exception ex) {
            LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
        }
    }

}
