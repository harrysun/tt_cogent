## Cogent Code Test
### Tech stack
- Java 8
- gradle, jacoco, jUnit, checkstyle, findbug
### Preparation
- Install JDK 8
- Install Gradle (Optional)
- Clone source code
```
git clone https://bitbucket.org/harrysun/tt_cogent/
```
- Change to the project folder as current directory
```
cd tt_cogent
```
- Unzip the zip file into current folder
### Build (Optional, when gradle is installed)
```
./gradlew clean build
```
### Run with gradle
```
syntax:
./gradlew run --args=<directory to scan>
e.g.
./gradlew run --args="'./Code Test'"/
```
### Run with java
```
syntax:
java -jar cogent.jar <directory to scan>
e.g.
java -jar cogent.jar "Code Test"
```

